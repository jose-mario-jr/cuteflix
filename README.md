[![CuteFlix Logo](logo-shadow.png)](https://cuteflix.vercel.app/)

# [CuteFlix](https://cuteflix.vercel.app/)

A website full of kitties, puppies and other cute ones, built in the React Imersion Week from Alura Technology Courses.
The layout is based on the NetFlix app.

This website allows you to:

- Browse cute videos
- Add cute videos

## Technologies used

- Modern Javascript
- React

## Screenshots

### Here you see some screenshots:

Main Page:
![](https://i.imgur.com/Sv39Csf.png)
Scrolling down...
![](https://i.imgur.com/MmOrmi7.png)

## Setup:

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.
