import config from "../config"

const URL_VIDEOS = `${config.URL_BACKEND_TOP}/VIDEOS`

async function create(objetoDoVideo) {
  const res = await fetch(`${URL_VIDEOS}?_embed=videos`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify(objetoDoVideo),
  })
  if (res.ok) {
    const json = await res.json()
    return json
  }
  throw new Error("Nao foi possivel cadastrar os dados!")
}

export default {
  create,
}
