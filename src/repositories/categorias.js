import config from "../config"

const URL_CATEGORIES = `${config.URL_BACKEND_TOP}/categorias`

async function getAll() {
  const res = await fetch(`${URL_CATEGORIES}`)
  if (res.ok) {
    const json = await res.json()
    return json
  }
  throw new Error("Nao foi possivel pegar os dados!")
}
async function getAllWithVideos() {
  const res = await fetch(`${URL_CATEGORIES}?_embed=videos`)
  if (res.ok) {
    const json = await res.json()
    return json
  }
  throw new Error("Nao foi possivel pegar os dados!")
}

export default {
  getAllWithVideos,
  getAll,
}
