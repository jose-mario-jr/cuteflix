import React from "react"
import logo from "../../assets/logo.png"
import "./Menu.css"
import Button from "../Button/index.js"
import { Link } from "react-router-dom"

function Menu() {
  return (
    <nav className="Menu">
      <Link to="/">
        <img className="Logo" src={logo} alt="CuteFlix logo" />
      </Link>

      <Button as={Link} className="ButtonLink" to="/cadastro/video">
        Novo Video
      </Button>
    </nav>
  )
}
export default Menu
