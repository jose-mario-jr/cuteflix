import React, { useState, useEffect } from "react"
import PageDefault from "../../../components/PageDefault"
import FormField from "../../../components/FormField"
import Button from "../../../components/Button"
import useForm from "../../../hooks/useForm"

function CadastroCategoria() {
  const valoresIniciais = {
    titulo: "",
    descricao: "",
    cor: "",
  }

  const { values, handleChange, clearForm } = useForm(valoresIniciais)

  const [categorias, setCategorias] = useState([])

  useEffect(() => {
    async function fetchData() {
      const URL_TOP = window.location.hostname.includes("localhost")
        ? "http://localhost:8080/categorias"
        : "" // depois de deploy, colocar a url certa
      const res = await fetch(URL_TOP)
      const json = await res.json()
      setCategorias([...json])
    }
    fetchData()
  }, [])

  return (
    <PageDefault>
      <h1>
        Cadastro de categoria:
        {values.titulo}
      </h1>

      <form
        onSubmit={(e) => {
          e.preventDefault()
          setCategorias([...categorias, values])

          clearForm()
        }}
      >
        <FormField
          label="Titulo da Categoria"
          type="text"
          value={values.titulo}
          name="titulo"
          onChange={handleChange}
        />

        <FormField
          label="Descricao"
          type="textarea"
          name="descricao"
          value={values.descricao}
          onChange={handleChange}
        />
        <FormField
          label="Cor"
          type="color"
          name="cor"
          value={values.cor}
          onChange={handleChange}
        />

        <Button>Cadastrar</Button>
      </form>

      {categorias.length === 0 && <div>Loading...</div>}

      <ul>
        {categorias.map((cat) => (
          <li key={`${cat.titulo}`}>{cat.titulo}</li>
        ))}
      </ul>
    </PageDefault>
  )
}
export default CadastroCategoria
