import React, { useState } from "react"
import styled from "styled-components"
import Minesweeper from "react-minesweeper"
import PageDefault from "../../components/PageDefault"

import "react-minesweeper/lib/minesweeper.css"

const ContainerMS = styled.div`
  /* display: flex; */
  text-align: center;
  /* flex-direction: row */
`

function Page404() {
  const [state, setState] = useState({
    msg: "GAME IN PROGRESS",
    minesweeperKey: 0,
    bombChance: 15,
    width: 10,
    height: 10,
  })
  const onWin = () => {
    setState({
      msg: "YOU WON!",
    })
  }
  const onLose = () => {
    setState({
      msg: "GAME OVER!",
    })
  }
  const restartMinesweeper = () => {
    setState((prevState) => ({
      minesweeperKey: prevState.minesweeperKey + 1,
      msg: "GAME IN PROGRESS",
    }))
  }
  const updateStateProp = (prop) => (e) => {
    const value = Number(e.target.value)
    setState((prevState) => {
      prevState[prop] = value
      prevState.minesweeperKey += 1
      return prevState
    })
  }
  const { msg, minesweeperKey, bombChance, height, width } = state

  return (
    <PageDefault paddingAll="0">
      <h1>
        <a href="https://github.com/oL-web/react-minesweeper">
          react-minesweeper
        </a>
      </h1>
      <p>{msg}</p>
      <ContainerMS className="container">
        <div className="minesweeper__header">
          <button className="minesweeper__restart" onClick={restartMinesweeper}>
            <span role="img" aria-label="restart-emoji">
              😃
            </span>
          </button>
        </div>
        <Minesweeper
          key={minesweeperKey}
          onWin={onWin}
          onLose={onLose}
          bombChance={bombChance / 100}
          width={width}
          height={height}
        />

        <div className="minesweeper__options">
          <div>
            <label htmlFor="bomb-chance">Bomb chance (0-100%): </label>
            <input
              onChange={updateStateProp("bombChance")}
              id="bomb-chance"
              type="number"
              max="100"
              min="0"
              defaultValue={bombChance}
            />
          </div>

          <div>
            <label htmlFor="fields-horizontally">Fields horizontally: </label>
            <input
              onChange={updateStateProp("width")}
              id="fields-horizontally"
              type="number"
              defaultValue={width}
            />
          </div>

          <div>
            <label htmlFor="fields-vertically">Fields vertically: </label>
            <input
              onChange={updateStateProp("height")}
              id="fields-vertically"
              type="number"
              defaultValue={height}
            />
          </div>
        </div>
      </ContainerMS>

      <footer>
        <p>
          Made by Michał Olejniczak. |
          <a href="https://ol-web.github.io">My GitHub page</a>
        </p>
        <p>Icons made by Freepik and Bogdan Rosu from www.flaticon.com </p>
      </footer>
    </PageDefault>
  )
}
export default Page404
